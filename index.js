const root = document.querySelector("#root");
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function isValidBook(book) {
    try {
        if (!book.author) {
            throw new Error('Відсутня властивість "author"');
        }
        if (!book.name) {
            throw new Error('Відсутня властивість "name"');
        }
        if (!book.price) {
            throw new Error('Відсутня властивість "price"');
        }

        return true;
    } catch (error) {
        console.error(`Помилка: ${error.message}`);

        return false;
    }
}

books.forEach(el => {
        if(!isValidBook(el)) {

            return;
        }
        root.innerHTML += `<ul>
                       <li>Author: ${el.author}</li>
                       <li>Name: ${el.name}</li>
                       <li>Price: ${el.price}</li>
                       </ul>`;
});